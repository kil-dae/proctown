﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Zones))]
public class ZonesEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Zones myScript = (Zones)target;
        if (GUILayout.Button("Generate"))
        {
            myScript.Generate();
        }
    }
}