﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Side {
    public Vector2 p1;
    public Vector2 p2;

    public Vector2 dir;
    public Vector2 normal;
    public Vector2 mergedNormal;

    public float length;

    public bool streetSide;

    public bool Intersect(Vector2 origin, Vector2 dir, out Vector2 pos) {
        dir += origin;
        float det = ((this.p1.x - this.p2.x) * (origin.y - dir.y) - (this.p1.y - this.p2.y) * (origin.x - dir.x));

        if(Mathf.Abs(det) <= float.Epsilon) {
            pos = Vector2.zero;
            return false;
        }

        float s1 = (this.p1.x * this.p2.y - this.p1.y * this.p2.x);
        float s2 = (origin.x * dir.y - origin.y * dir.x);

        float iX = (s1 * (origin.x - dir.x) - (this.p1.x - this.p2.x) * s2) / det;

        float iY = (s1 * (origin.y - dir.y) - (this.p1.y - this.p2.y) * s2) / det;

        pos = new Vector2(iX, iY);

        float l = (this.p2 - this.p1).magnitude;

        return (((pos - this.p1).magnitude < l) && ((pos - this.p2).magnitude < l));
    }
}

public struct Polygon {
    public List<Side> sides;
    public Vector2 center;
    public bool closed;

    public ZoneType type; // TODO: handle this differently

    public void CalculateVectors() {
        if(!this.closed) return;
        for(int s = 0; s < this.sides.Count; ++s) {
            Side side = this.sides[s];
            int next = s + 1;
            if(next >= this.sides.Count) next = 0;
            side.length = (side.p2 - side.p1).magnitude;
            side.dir = (this.sides[s].p2 - this.sides[s].p1).normalized;
            side.normal = new Vector2(side.dir.y, -side.dir.x);
            Vector2 nextDir = (this.sides[next].p2 - this.sides[next].p1).normalized; // duplicate calculations
            Vector2 nextNormal = new Vector2(nextDir.y, -nextDir.x);
            side.mergedNormal = (side.normal + nextNormal).normalized;
            this.sides[s] = side;
        }
    }
    public bool Slice(Vector2 origin, Vector2 dir, out Polygon leftSlice, bool street = false) {
        bool firstFound = false;
        bool secondFound = false;
        int first = 0;
        int second = 1;
        Vector2 firstIntersection = Vector2.zero;
        Vector2 secondIntersection = Vector2.zero;
        Vector2 tempIntersection;
        leftSlice = new Polygon();
        leftSlice.sides = new List<Side>();
        leftSlice.closed = true;
        leftSlice.center = center;
        leftSlice.type = type;
        for(int s=0; s<sides.Count; ++s) {
            if(sides[s].Intersect(origin, dir, out tempIntersection)) { // cannot use ternary operator on out variable?
                if(firstFound) {
                    second = s;
                    secondFound = true;
                    secondIntersection = tempIntersection;
                    break;
                } else {
                    first = s;
                    firstFound = true;
                    firstIntersection = tempIntersection;
                }
            }
        }

        if(!secondFound || first == second) {
            return false;
        }

        // TODO: swap first and second if second intersection - first intersection is opposite to dir
        // this check can possibly put slice on wrong side too
        if(first > second) {
            int temp = second;
            second = first;
            first = temp;
        }

        Side leftStart = sides[first];
        leftStart.p1 = firstIntersection;
        leftSlice.sides.Add(leftStart);

        for(int s = first+1; s < second; ++s) { // double check behavior on least sides corner
            leftSlice.sides.Add(sides[s]);
        }

        // currently replacing sliced edges with new ones, could also adjust
        Side leftEnd = sides[second];
        leftEnd.p2 = secondIntersection;
        leftSlice.sides.Add(leftEnd);
        Side leftConnect = new Side();
        leftConnect.streetSide = street;
        leftConnect.p1 = leftEnd.p2;
        leftConnect.p2 = leftStart.p1;
        leftSlice.sides.Add(leftConnect);

        Side rightStart = sides[first];
        rightStart.p2 = firstIntersection;
        Side rightEnd = sides[second];
        rightEnd.p1 = secondIntersection;
        Side rightConnect = new Side();
        rightConnect.streetSide = street;
        rightConnect.p1 = rightStart.p2;
        rightConnect.p2 = rightEnd.p1;

        // TODO: optimize most list operations
        sides.RemoveRange(first, second - first + 1);
        sides.Insert(first, rightEnd);
        sides.Insert(first, rightConnect);
        sides.Insert(first, rightStart);

        return true;
    }
}