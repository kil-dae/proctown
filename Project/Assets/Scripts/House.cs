﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour {
    public static int debugCount = 0;

    public static House Generate(Polygon surface, Material material, HouseType type) {
        if(surface.sides.Count < 4) return null;

        Side previous = surface.sides[surface.sides.Count - 1];
        Vector2 avg = Vector2.zero; // can calc elsewhere but more efficient here now
        for(int s = 0; s < surface.sides.Count; ++s) {
            avg += surface.sides[s].p1;
            Vector2 diff = surface.sides[s].p1 - previous.p2;
            Vector3 cross = Vector3.Cross(new Vector3(previous.dir.x, 0, previous.dir.y), new Vector3(surface.sides[s].dir.x, 0, surface.sides[s].dir.y));
            if(Mathf.Abs(diff.x) > 0.1f || Mathf.Abs(diff.y) > 0.1f || cross.y < 0) return null; // ignore open and concave polygons
            previous = surface.sides[s];
        }
        avg /= surface.sides.Count;


        GameObject g = new GameObject("Haus " + debugCount);
        ++debugCount;
        g.transform.position = new Vector3(avg.x, 0, avg.y);

        for(int s = 0; s < surface.sides.Count; ++s) {
            var side = surface.sides[s];
            side.p1 -= avg;
            side.p2 -= avg;
            surface.sides[s] = side;
        }

        int floors = type.FloorsCount;

        //float floorHeight = Random.Range(1.8f, 2.4f);
        float floorHeight = type.StoryHeight;

        float roofHeight = floorHeight * floors;
        float roofTop = roofHeight + Random.Range(floorHeight, floorHeight * 1.5f);

        Mesh m = new Mesh();
        List<Vector3> vs = new List<Vector3>(surface.sides.Count * 3);
        List<Vector3> ns = new List<Vector3>(surface.sides.Count * 3);
        foreach(var side in surface.sides) {
            Vector3 v1 = new Vector3(side.p1.x, roofHeight, side.p1.y);
            Vector3 v2 = new Vector3(side.p2.x, roofHeight, side.p2.y);
            Vector3 vb = new Vector3(0, roofTop, 0);
            vs.Add(v1);
            vs.Add(v2);
            vs.Add(vb);
            Vector3 normal = Vector3.Cross(v1 - vb, v2 - vb).normalized;
            ns.Add(normal);
            ns.Add(normal);
            ns.Add(normal);
        }

        for(int f = 0; f < floors; ++f) {
            float floorY = f * floorHeight;
            float ceilingY = (f + 1) * floorHeight;
            foreach(var side in surface.sides) {
                vs.Add(new Vector3(side.p1.x, floorY, side.p1.y));
                vs.Add(new Vector3(side.p2.x, floorY, side.p2.y));
                vs.Add(new Vector3(side.p1.x, ceilingY, side.p1.y));
                vs.Add(new Vector3(side.p1.x, ceilingY, side.p1.y));
                vs.Add(new Vector3(side.p2.x, floorY, side.p2.y));
                vs.Add(new Vector3(side.p2.x, ceilingY, side.p2.y));
                ns.Add(new Vector3(-side.normal.x, 0, -side.normal.y));
                ns.Add(new Vector3(-side.normal.x, 0, -side.normal.y));
                ns.Add(new Vector3(-side.normal.x, 0, -side.normal.y));
                ns.Add(new Vector3(-side.normal.x, 0, -side.normal.y));
                ns.Add(new Vector3(-side.normal.x, 0, -side.normal.y));
                ns.Add(new Vector3(-side.normal.x, 0, -side.normal.y));
            }
        }
        List<int> inds = new List<int>(vs.Count);
        for(int i = 0; i < vs.Count; ++i) inds.Add(i); // inefficient indexing, just for testing

        m.SetVertices(vs);
        m.SetNormals(ns);
        m.SetIndices(inds.ToArray(), MeshTopology.Triangles, 0);
        g.AddComponent<MeshFilter>().mesh = m;
        g.AddComponent<MeshRenderer>().material = material;

        float segmentWidth = type.SegmentWidth;

        House h = g.AddComponent<House>();

        int doorSide = Random.Range(0, surface.sides.Count);

        for(int s = 0; s < surface.sides.Count; ++s) {
            var side = surface.sides[s];
            GameObject facadeOrigin = new GameObject("facade " + s);
            facadeOrigin.transform.SetParent(g.transform, false);
            var dir = (side.p1 - side.p2).normalized;
            facadeOrigin.transform.localPosition = new Vector3(side.p2.x, 0, side.p2.y);
            facadeOrigin.transform.localEulerAngles = new Vector3(0, -Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg, 0);

            float length = (side.p1 - side.p2).magnitude; // TODO: optimize

            int segments = (int)(length / segmentWidth);
            if(segments == 0) segments = 1;

            float segmentLength = length / segments;

            int doorSegment = 0; // temp fix
            if(s == doorSide) {
                doorSegment = Random.Range(0, segments);
            }

            for(int f = 0; f < floors; ++f) {
                //var hor = Instantiate(f == floors - 1 ? type.TopDivider : type.HorizontalDivider, facadeOrigin.transform);
                //hor.transform.localScale = new Vector3(length, 1, 1);
                //hor.transform.localPosition = new Vector3(0, (f + 1) * floorHeight, 0);

                for(int seg = 0; seg < segments; ++seg) {
                    var vert = Instantiate(type.VerticalDivider, facadeOrigin.transform);
                    vert.transform.localScale = new Vector3(1, floorHeight, 1);
                    vert.transform.localPosition = new Vector3(seg * segmentLength, f * floorHeight, 0);


                    var hor = Instantiate(f == floors - 1 ? type.TopDivider : type.HorizontalDivider, facadeOrigin.transform);
                    hor.transform.localScale = new Vector3(segmentLength, 1, 1);
                    hor.transform.localPosition = new Vector3(seg * segmentLength, (f + 1) * floorHeight, 0);

                    if(Random.Range(0, 5) <= 2) {
                        if(f == 0 && s == doorSide && seg == doorSegment) continue;
                        var window = Instantiate(type.Window, facadeOrigin.transform);
                        window.transform.localPosition = new Vector3(seg * segmentLength + segmentLength * 0.5f, f * floorHeight, 0);
                    }
                }

                
            }

            if(s == doorSide) {
                var door = Instantiate(type.Door, facadeOrigin.transform);
                door.transform.localPosition = new Vector3(doorSegment * segmentLength + segmentLength * 0.5f, 0, 0);
            }
            //else if(Random.Range(0, 3) != 0) {
            //    int windowSegment = Random.Range(0, segments);
            //    var window = Instantiate(type.Window, facadeOrigin.transform);
            //    window.transform.localPosition = new Vector3(windowSegment * segmentLength + segmentLength * 0.5f, 0, 0);
            //}
        }


        for(int s = 0; s < surface.sides.Count; ++s) { // TEMP FIX for visualization
            var side = surface.sides[s];
            side.p1 += avg;
            side.p2 += avg;
            surface.sides[s] = side;
        }

        return h;
    }
}
