﻿using csDelaunay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zones : MonoBehaviour {
    
    public bool renderZones = true;
    public bool renderStreet = true;

    public bool liveUpdate = true;
    public bool generateHouses = true;

    private float reach = 500.0f;
    private int circles = 3;
    private float streetOffset = 5.0f;
    private float streetDivision = 8.0f;
    private float innerStreetWidth = 2.0f;
    //public float randomization = 0.5f;
    private Voronoi voronoi;
    private List<Polygon> zones;
    private List<Polygon> zoneSubdivs;
    private List<GameObject> houses;

    //private List<Mesh> debugMeshes;

    private bool generated = false;

    public AnimationCurve sizesCurve;
    public AnimationCurve randomizationCurve;

    [HideInInspector]
    public GameObject dbtext;

    private int seed = 0;
    public bool randomize = false;

    public Material material;

    public TownType storedTown;

    // Use this for initialization
    void Start ()
    {
        Generate();
    }

    void PerpendicularSubdiv(ref List<Polygon> polysIn, ref List<Polygon> polysOut) {
        foreach(var originalZone in polysIn) {
            if(!originalZone.closed || originalZone.sides.Count == 0 || originalZone.type.houseTypes.Count == 0) continue; // strange exception, track down later

            streetDivision = originalZone.type.AverageHouseSize;

            Polygon zone = originalZone;

            Side longest = zone.sides[0];
            float length = longest.length;
            int longestIndex = 0;
            for(int s = 1; s < zone.sides.Count; ++s) {
                if(zone.sides[s].length > length) {
                    longest = zone.sides[s];
                    length = zone.sides[s].length;
                    longestIndex = s;
                }
            }

            int previous = (longestIndex == 0) ? zone.sides.Count - 1 : longestIndex - 1;
            int next = (longestIndex == zone.sides.Count - 1) ? 0 : longestIndex + 1;

            float maxCornerDP = -0.34f; //110 deg
            //maxCornerDP = -2.0f; // TODO: remove temp measure
            int streets = Mathf.Max(2, (int)length / (int)streetDivision);
            float spacing = length / (streets - 1);
            int start = 0;
            bool endCorner = false;
            if(Vector2.Dot(longest.dir, -zone.sides[previous].dir) > maxCornerDP) { start = 1; }
            if(Vector2.Dot(-longest.dir, zone.sides[next].dir) > maxCornerDP) { --streets; endCorner = true; }

            Polygon lastSlice = new Polygon();
            lastSlice.sides = new List<Side>();
            lastSlice.closed = true;
            lastSlice.type = originalZone.type;

            for(int s = start; s < streets; ++s) {
                Vector2 pos = longest.p1 + longest.dir * (spacing * s + Random.Range(-spacing * 0.25f, spacing * 0.25f));

                Polygon slice;
                if(zone.Slice(pos, longest.normal, out slice, true)) {
                    if(s != start) {
                        polysOut.Add(lastSlice);
                    }
                    lastSlice = slice;
                } else if(s != start) {
                    polysOut.Add(zone);
                    zone = lastSlice;
                    if(zone.Slice(pos, longest.normal, out slice, true)) {
                        lastSlice = slice;
                    }
                }
            }
            polysOut.Add(zone);
            polysOut.Add(lastSlice);
        }
    }

    public void Generate()
    {
        if (!randomize) Random.InitState(storedTown.seed);
        Debug.Log("Seed: " + Random.seed); // deprecated but there doesn't seem to be a proper replacement

        streetOffset = storedTown.streetWidth;
        circles = storedTown.circleCount;
        reach = storedTown.size;

        sizesCurve = storedTown.circleSizesCurve;
        randomizationCurve = storedTown.circleRandomizationCurve;

        //int count = Random.Range(10, 30);
        List<Vector2> points = new List<Vector2>();
        points.Add(new Vector2(reach * 0.5f, reach * 0.5f));
        float radiusStep = reach * 0.5f / circles;
        float circleSlicing = storedTown.circleSlicing.Evaluate();
        int outercount = 0;
        for (int c = 1; c <= circles; ++c)
        {
            if(c == circles) outercount = points.Count;
            float curve = ((float)c) / circles;
            float randomization = randomizationCurve.Evaluate(curve);
            curve = sizesCurve.Evaluate(curve);
            float rad = radiusStep * curve * circles;
            int slices = 2 + (int)(c * circleSlicing);
            float interval = Mathf.PI * 2.0f / slices;
            float rInterval = randomization * 0.5f * interval;
            float rRad = randomization * 0.5f * radiusStep;
            float rAngOffset = Random.Range(0.0f,Mathf.PI * 2.0f)*randomization;
            for (int s = 0; s < slices; ++s)
            {
                float ang = rAngOffset + s * interval + Random.Range(-rInterval, rInterval);
                float r = rad + Random.Range(-rRad, rRad);
                points.Add(new Vector2(reach * 0.5f + r * Mathf.Cos(ang), reach * 0.5f - r * Mathf.Sin(ang)));
            }
        }
        Rectf bounds = new Rectf(0, 0, reach, reach);
        voronoi = new Voronoi(points, bounds);
        zones = new List<Polygon>();
        bool debugText = false;
        int kvpcount = 0;
        foreach (var kvp in voronoi.SitesIndexedByLocation)
        {
            if(kvpcount >= outercount) break;
            ++kvpcount;
            
            // store voronoi cell as Zone
            Site site = kvp.Value;
            Polygon zone = new Polygon();
            zone.sides = new List<Side>();
            zone.closed = true;
            Vector2 center = site.Coord;
            // store edges

            //if(kvpcount <= 1) zone.type = storedTown.zoneTypes[0];
            //else {
                zone.type = storedTown.zoneTypes[Mathf.Min(storedTown.zoneTypes.Count - 1, kvpcount - 1)];
            //}
            
            foreach (var edge in site.Edges)
            {
                if (edge.ClippedEnds != null)
                {
                    Side facade = new Side();
                    facade.p1 = edge.ClippedEnds[LR.LEFT];
                    facade.p2 = edge.ClippedEnds[LR.RIGHT];
                    facade.streetSide = true;
                    zone.sides.Add(facade);
                } else {
                    zone.closed = false;
                }
            }
            // flip start and end if they're not clockwise
            for(int s=0; s<zone.sides.Count; ++s)
            {
                if (Vector3.Cross(new Vector3(zone.sides[s].p1.x - center.x, 0, zone.sides[s].p1.y - center.y), new Vector3(zone.sides[s].p2.x - center.x, 0, zone.sides[s].p2.y - center.y)).y < 0)
                {
                    Side flipped = zone.sides[s];
                    flipped.p1 = zone.sides[s].p2;
                    flipped.p2 = zone.sides[s].p1;
                    zone.sides[s] = flipped;
                }
            }
            // store angles from center
            float[] angles = new float[zone.sides.Count];
            angles[0] = 0;
            float start = Mathf.Atan2(zone.sides[0].p1.y - center.y, zone.sides[0].p1.x - center.x);
            for (int s = 1; s < zone.sides.Count; ++s)
            {
                float diff = Mathf.Atan2(zone.sides[s].p1.y - center.y, zone.sides[s].p1.x - center.x) - start;
                if (diff > 0) diff = -Mathf.PI * 2.0f + diff;
                angles[s] = -diff;
            }
            //float currentDiff = 0;
            float smallest = float.MaxValue;
            int smallestIndex = 0;

            for (int ind = 1; ind < zone.sides.Count; ++ind) // TODO: leave out last side
            {
                //bool found = false;
                for(int s=ind; s<zone.sides.Count; ++s)
                {
                    if(angles[s]>angles[ind-1]+float.Epsilon /*just in case*/ && angles[s] < smallest)
                    {
                        smallestIndex = s;
                        smallest = angles[s];
                        //found = true;
                    }
                }
                //if (found)
                //{
                float swapAngle = angles[ind];
                angles[ind] = angles[smallestIndex];
                angles[smallestIndex] = swapAngle;

                Side swapSide = zone.sides[ind];
                zone.sides[ind] = zone.sides[smallestIndex];
                zone.sides[smallestIndex] = swapSide;

                smallest = float.MaxValue;

                //}
            }
            if (debugText)
            {
                for (int s = 0; s < zone.sides.Count; ++s)
                {
                    var t = Instantiate(dbtext);
                    t.transform.position = new Vector3(zone.sides[s].p1.x, 0, zone.sides[s].p1.y);
                    t.GetComponent<TextMesh>().text = s.ToString();
                }
                debugText = false;
            }
            zones.Add(zone);
        }

        //calculate lengths
        foreach (var zone in zones)
        {
            if (!zone.closed) continue;
            for (int s = 0; s < zone.sides.Count; ++s)
            {
                Side side = zone.sides[s];
                side.length = (side.p2 - side.p1).magnitude;
                zone.sides[s] = side;
            }
        }

        // Remove too short segments
        foreach (var zone in zones)
        {
            if (!zone.closed) continue;
            for (int s = 0; s < zone.sides.Count; ++s)
            {
                if (zone.sides[s].length < streetOffset * 2.0f)
                {
                    int previous = (s == 0) ? zone.sides.Count - 1 : s - 1;
                    int next = (s == zone.sides.Count - 1) ? 0 : s + 1;
                    Vector2 avg = (zone.sides[s].p1 + zone.sides[s].p2) * 0.5f;
                    Side p = zone.sides[previous];
                    Side n = zone.sides[next];
                    p.p2 = avg;
                    n.p1 = avg;
                    zone.sides[previous] = p;
                    zone.sides[next] = n;
                    zone.sides.RemoveAt(s);
                    // TODO: recalc normals, length, etc
                }
            }
        }

        //TODO: store roads

        //calculate normals
        foreach (var zone in zones)
        {
            zone.CalculateVectors();
        }

        // offset by normals
        foreach (var zone in zones)
        {
            if (!zone.closed) continue;
            for (int s = 0; s < zone.sides.Count; ++s)
            {
                Side side = zone.sides[s];
                int next = s + 1;
                if (next >= zone.sides.Count) next = 0;
                side.p2 += side.mergedNormal * streetOffset;
                Side nextSide = zone.sides[next];
                nextSide.p1 += side.mergedNormal * streetOffset;
                zone.sides[s] = side;
                zone.sides[next] = nextSide;

                // TODO: recalc normals, length, etc
            }
        }

        // find too small and intersecting sides
        foreach (var zone in zones)
        {
            if (!zone.closed) continue;
            for (int s = 0; s < zone.sides.Count; ++s)
            {
                if (zone.sides[s].length < streetOffset * 2.0f)
                {
                    int previous = (s == 0) ? zone.sides.Count - 1 : s - 1;
                    int next = (s == zone.sides.Count - 1) ? 0 : s + 1;
                    Vector2 avg = (zone.sides[s].p1 + zone.sides[s].p2) * 0.5f;
                    Side p = zone.sides[previous];
                    Side n = zone.sides[next];
                    p.p2 = avg;
                    n.p1 = avg;
                    zone.sides[previous] = p;
                    zone.sides[next] = n;
                    zone.sides.RemoveAt(s);
                }
            }
        }

        // TODO: find concaves

        //recalculate normals
        foreach (var zone in zones)
        {
            zone.CalculateVectors();
        }

        List<Polygon> zoneStrips = new List<Polygon>();
        PerpendicularSubdiv(ref zones, ref zoneStrips);

        foreach(var zone in zoneStrips) zone.CalculateVectors();

        zoneSubdivs = new List<Polygon>();
        PerpendicularSubdiv(ref zoneStrips, ref zoneSubdivs);


        //calculate normals
        foreach(var zone in zoneSubdivs) {
            zone.CalculateVectors();
        }

        //innerStreetWidth = storedTown.zoneTypes[0].HouseSpacing;

        // offset by normals
        foreach(var zone in zoneSubdivs) {
            if(!zone.closed) continue;
            innerStreetWidth = zone.type.HouseSpacing;
            for(int s = 0; s < zone.sides.Count; ++s) {
                Side side = zone.sides[s];
                int next = s + 1;
                if(next >= zone.sides.Count) next = 0;
                side.p2 += side.mergedNormal * innerStreetWidth;
                Side nextSide = zone.sides[next];
                nextSide.p1 += side.mergedNormal * innerStreetWidth;
                zone.sides[s] = side;
                zone.sides[next] = nextSide;

                zone.CalculateVectors();
            }
        }

        // find too small and intersecting sides
        foreach(var zone in zoneSubdivs) {
            if(!zone.closed) continue;
            for(int s = 0; s < zone.sides.Count; ++s) {
                if(zone.sides[s].length < innerStreetWidth * 2.0f) {
                    int previous = (s == 0) ? zone.sides.Count - 1 : s - 1;
                    int next = (s == zone.sides.Count - 1) ? 0 : s + 1;
                    Vector2 avg = (zone.sides[s].p1 + zone.sides[s].p2) * 0.5f;
                    Side p = zone.sides[previous];
                    Side n = zone.sides[next];
                    p.p2 = avg;
                    n.p1 = avg;
                    zone.sides[previous] = p;
                    zone.sides[next] = n;
                    zone.sides.RemoveAt(s);
                }
            }
        }

        if(generated) {
            foreach(GameObject house in houses) {
                Destroy(house);
            }
        }

        House.debugCount = 0;
        houses = new List<GameObject>();
        if(generateHouses) {
            for(int z = 0; z < zoneSubdivs.Count; ++z) {
                //new GameObject().AddComponent<House>().Generate(zoneSubdivs[z], material); // memory management nightmare
                //MakeHouse(zoneSubdivs[z]);
                var house = House.Generate(zoneSubdivs[z], material, zoneSubdivs[z].type.houseTypes[Random.Range(0, zoneSubdivs[z].type.houseTypes.Count)]);
                if(house) houses.Add(house.gameObject);
            }
        }

        generated = true;
    }

    private void OnDrawGizmos()
    {
        if (generated && voronoi!=null)
        {
            if (renderStreet)
            {
                Gizmos.color = Color.white;
                foreach (var kvp in voronoi.SitesIndexedByLocation)
                {
                    Site zone = kvp.Value;
                    //Gizmos.DrawSphere(new Vector3(kvp.Key.x, 0, kvp.Key.y), 10.0f);
                    foreach (var edge in zone.Edges)
                    {
                        //if (edge.LeftVertex != null && edge.RightVertex != null)
                        if (edge.ClippedEnds != null)
                            Gizmos.DrawLine(new Vector3(edge.ClippedEnds[LR.LEFT].x, 0, edge.ClippedEnds[LR.LEFT].y), new Vector3(edge.ClippedEnds[LR.RIGHT].x, 0, edge.ClippedEnds[LR.RIGHT].y));
                        //Gizmos.DrawLine(new Vector3(edge.LeftVertex.x, 0, edge.LeftVertex.y), new Vector3(edge.RightVertex.x, 0, edge.RightVertex.y));

                    }
                }
            }
            if (renderZones)
            {
                foreach(var zone in zoneSubdivs) {
                    if(!zone.closed) continue;
                    //float c = 0.0f;
                    Gizmos.color = Color.white;
                    if(zone.sides.Count < 4) continue;
                    foreach(var facade in zone.sides) {
                        //c += 0.05f;
                        //Gizmos.color = new Color(c, c, c);
                        Gizmos.DrawLine(new Vector3(facade.p1.x, 0, facade.p1.y), new Vector3(facade.p2.x, 0, facade.p2.y));
                    }
                }
                //Random.InitState(42);
                //foreach(var mesh in debugMeshes) {
                //    Gizmos.color = Random.ColorHSV(0, 1.0f, 0.8f, 1.0f, 0.9f, 1.0f, 0.5f, 0.51f);
                //    Gizmos.DrawMesh(mesh);
                //}
            }
            //Gizmos.color = Color.white;
            //foreach(var edge in voronoi.Edges) {
            //    //if (edge.LeftVertex != null && edge.RightVertex != null)
            //    Gizmos.DrawSphere(new Vector3(edge.RightSite.x, 0, edge.RightSite.y), 1.0f);
            //    //if (edge.ClippedEnds != null)
            //    //    Gizmos.DrawLine(new Vector3(edge.ClippedEnds[LR.LEFT].x, 0, edge.ClippedEnds[LR.LEFT].y), new Vector3(edge.ClippedEnds[LR.RIGHT].x, 0, edge.ClippedEnds[LR.RIGHT].y));
            //    //Gizmos.DrawLine(new Vector3(edge.LeftVertex.x, 0, edge.LeftVertex.y), new Vector3(edge.RightVertex.x, 0, edge.RightVertex.y));
            //}
            //Gizmos.color = Color.white;
            //foreach(var kvp in voronoi.SitesIndexedByLocation) {
            //    //Site zone = kvp.Value;
            //    Gizmos.DrawSphere(new Vector3(kvp.Key.x, 0, kvp.Key.y), 10.0f);
            //    //float c = 0.0f;
            //    //foreach(var edge in zone.Edges) {
            //    //    //if (edge.LeftVertex != null && edge.RightVertex != null)
            //    //    if(edge.ClippedEnds != null) {
            //    //        Gizmos.color = new Color(c, c, c);
            //    //        Gizmos.DrawLine(new Vector3(edge.ClippedEnds[LR.LEFT].x, 0, edge.ClippedEnds[LR.LEFT].y), new Vector3(edge.ClippedEnds[LR.RIGHT].x, 0, edge.ClippedEnds[LR.RIGHT].y));
            //    //    }
            //    //    c += 0.15f;
            //    //    //Gizmos.DrawLine(new Vector3(edge.LeftVertex.x, 0, edge.LeftVertex.y), new Vector3(edge.RightVertex.x, 0, edge.RightVertex.y));
            //    //}
            //    break;
            //}
        }
    }

    private void Update() {
        if(liveUpdate && !generateHouses)
            Generate();
    }
}
