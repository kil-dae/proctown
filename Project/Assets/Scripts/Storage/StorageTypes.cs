﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum DistributionType {
    Fixed, Range, Curve
}

[System.Serializable]
public class DistributedValue : System.Object {
    public DistributionType Distribution = DistributionType.Range;
    public float Min = 0.5f;
    public float Max = 1.5f;
    public AnimationCurve Curve = AnimationCurve.Linear(0, 0.5f, 1, 1.5f);
    public DistributedValue(float min, float max, AnimationCurve curve, DistributionType type) {
        Distribution = type;
        Min = min;
        Max = max;
        Curve = curve;
    }
    public DistributedValue(float min, float max, DistributionType type = DistributionType.Range) {
        Distribution = type;
        Min = min;
        Max = max;
        Curve = AnimationCurve.Linear(0, min, 1, max);
    }
    public DistributedValue(float value, DistributionType type = DistributionType.Fixed) {
        Distribution = type;
        Min = value;
        Max = value * 2;
        Curve = AnimationCurve.Linear(0, Min, 1, Max);
    }
    public DistributedValue(AnimationCurve curve, DistributionType type = DistributionType.Curve) {
        Distribution = type;
        Min = curve.keys[0].value;
        Max = curve.keys[curve.keys.Length].value;
        Curve = curve;
    }

    public float Evaluate() {
        switch(Distribution) {
        case DistributionType.Fixed:
        default:
            return Min;
            break;
        case DistributionType.Range:
            return Random.Range(Min, Max);
            break;
        case DistributionType.Curve:
            return Curve.Evaluate(Random.value);
            break;
        }
    }

    public int EvaluateInt() {
        switch(Distribution) {
        case DistributionType.Fixed:
        default:
            return (int)Min;
            break;
        case DistributionType.Range:
            return Random.Range((int)Min, (int)Max+1);
            break;
        case DistributionType.Curve:
            return (int)Curve.Evaluate(Random.value);
            break;
        }
    }

    public static implicit operator float(DistributedValue value) {
        return value.Evaluate();
    }
    public static implicit operator int(DistributedValue value) {
        return value.EvaluateInt();
    }
}


[CustomPropertyDrawer(typeof(DistributedValue))]
public class DistributedValueDrawer : PropertyDrawer {
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        var typeProperty = property.FindPropertyRelative("Distribution");
        DistributionType type = (DistributionType)typeProperty.enumValueIndex;
        return base.GetPropertyHeight(property, label) + ((type == DistributionType.Curve) ? 176 : 16);
    }

    override public void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        //DistributedValue val = attribute as DistributedValue;

        //float min = val.Min;
        //EditorGUI.FloatField(position, "Cool float", val.Min);
        //EditorGUI.Slider(position, val.Min, 0.0f, 2.0f);


        var typeProperty = property.FindPropertyRelative("Distribution");
        DistributionType type = (DistributionType)typeProperty.enumValueIndex;

        float lineHeight = 16.0f;

        Rect totalPos = position;
        //totalPos.height = 300;// (type == DistributionType.Curve) ? Mathf.Min(position.width * 0.6f, 200) + lineHeight * 2 : lineHeight * 2;
        
        //EditorGUI.BeginProperty(totalPos, GUIContent.none, property);

        // Draw label
        Rect enumPosition = EditorGUI.PrefixLabel(new Rect(position.x, position.y, position.width, lineHeight), GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        var minProperty = property.FindPropertyRelative("Min");
        var maxProperty = property.FindPropertyRelative("Max");

        EditorGUI.PropertyField(enumPosition, typeProperty, GUIContent.none);

        if(type == DistributionType.Fixed) {
            Rect minFieldRect = new Rect(position.x + position.width * 0.5f, position.y + lineHeight, position.width * 0.5f, lineHeight);
            EditorGUI.PropertyField(minFieldRect, property.FindPropertyRelative("Min"), GUIContent.none);
        } else {
            Rect minFieldRect = new Rect(position.x, position.y + lineHeight, position.width * 0.5f, lineHeight);
            Rect maxFieldRect = minFieldRect;
            minFieldRect.width -= 2;
            maxFieldRect.x += maxFieldRect.width;
            EditorGUI.PropertyField(minFieldRect, minProperty, GUIContent.none);
            EditorGUI.PropertyField(maxFieldRect, maxProperty, GUIContent.none);
        }

        if(type == DistributionType.Curve) {
            float min = Mathf.Min(minProperty.floatValue, maxProperty.floatValue);
            float max = Mathf.Max(minProperty.floatValue, maxProperty.floatValue);
            EditorGUI.CurveField(new Rect(position.x, position.y + lineHeight * 2, position.width, /*Mathf.Min(position.width * 0.6f, 200)*/160), property.FindPropertyRelative("Curve"), Color.red, new Rect(0, min, 1, max - min), GUIContent.none);
        }

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("Max"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;
        //EditorGUI.EndProperty();
    }
}