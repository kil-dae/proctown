﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "HouseType", menuName = "TownGen/House", order = 3)]
public class HouseType : ScriptableObject {
    public DistributedValue StoryHeight = new DistributedValue(1.9f, 2.2f);

    public DistributedValue SegmentWidth = new DistributedValue(1.5f, 2.5f);

    public DistributedValue FloorsCount = new DistributedValue(1, 2);

    [Header("Assets")]
    public GameObject Door;
    public GameObject Window;
    public GameObject VerticalDivider;
    public GameObject HorizontalDivider;
    public GameObject TopDivider;
}