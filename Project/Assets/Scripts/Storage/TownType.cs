﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TownType", menuName = "TownGen/Town", order = 1)]
public class TownType : ScriptableObject {
    public int seed = 0;

    public DistributedValue size = new DistributedValue(400, 600);

    public DistributedValue circleCount = new DistributedValue(4);
    public DistributedValue circleSlicing = new DistributedValue(3);
    public AnimationCurve circleSizesCurve;
    public AnimationCurve circleRandomizationCurve;

    public float streetWidth = 5.0f;

    public List<ZoneType> zoneTypes = new List<ZoneType>(1);
}
