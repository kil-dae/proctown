﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "ZoneType", menuName = "TownGen/Zone", order = 2)]
public class ZoneType : ScriptableObject {
    public DistributedValue AverageHouseSize = new DistributedValue(8, 12);
    public DistributedValue HouseSpacing = new DistributedValue(1.5f, 2.5f);

    public List<HouseType> houseTypes = new List<HouseType>(1);
}