﻿using System.Collections;

namespace csDelaunay {
	public interface ICoord {

		UnityEngine.Vector2 Coord {get;set;}
	}
}